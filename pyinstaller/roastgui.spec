# -*- mode: python -*-
a = Analysis(['../roastgui.py'],
             
             # Paths to where used python modules can be found.
             pathex=['../lib',
                     '../QtGUI',
                     '../QtGUI/designer'],
                     
             hiddenimports=[],
             hookspath=None)
             
pyz = PYZ(a.pure)

exe = EXE(pyz,
          a.scripts,
          exclude_binaries=1,
          name=os.path.join('build/pyi.linux2/roastgui', 'roastgui'),
          debug=False,
          strip=None,
          upx=True,
          console=True )
#
# # #
# Construct roast executables for packaging.

roastdir = os.listdir('../lib/roast')
roastdir = filter(lambda f: f.find('.c') == -1 and 
                       f.find('.h') == -1 and
                       f.find('Makefile') and
                       f.find('.git'), roastdir)
roastexecs = []

roastpath = os.path.join('lib', 'roast')

for p in roastdir:

    app_path = os.path.join(roastpath, p)
    rel_path = os.path.join('..', app_path)
    
    tuple = (app_path, rel_path, 'DATA')
    roastexecs.append(tuple)

coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               roastexecs,
               strip=None,
               upx=True,
               name=os.path.join('dist', 'roastgui'))
