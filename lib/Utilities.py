"""
Utilities.py

Contains assorted functions and classes 
that have no logically connected home (yet.)

@author Vincent Schramer
"""
import os
import string
import re
from threading import Thread


def find_files(path, substr):
    """
    Finds files located in the specified path with the specified substring.
    """
    # Handle invalid path.
    if not os.access(path, os.F_OK | os.R_OK):
        return []
            
        
    files = os.listdir(path);
        
    ext_finder = ExtFilter(substr)
        
    files = filter(ext_finder, files)
        
    return files

class ExtFilter:
    """
    Functor that returns false if the specified ext is not found.
    """
    
    def __init__(self, ext):
        self.ext = ext
        
    def __call__(self, item):
        if item.find(self.ext) == -1:
            return False
        else:
            return True
            
class LineReaderThread(Thread):
    """
    This thread is intended to continuously read lines from an output
    stream of a Popen process.
    """
    
    def __init__(self, popen, stream, callback):
        """
        Constructor
        
        @param Popen popen
            The Popen object to poll for close.
        @param File stream
            The output stream to read lines from
        @param Func(line) callback
            Function reference that handles lines from the process.
        """
        Thread.__init__(self)
        
        self.popen = popen
        self.stream = stream
        self.callback = callback
        
    def run(self):
        
        # While the process does not have a return code.
        while self.popen.poll() == None:
            
            # Read a line
            line = self.stream.readline()
            
            # Send the line to the callback function.
            self.callback(line)


def parse_guid_tree_names(guid_tree):
    """
    Given a guid tree such as (((human chimp) galago) (mouse rat4)),
    this will return a list of the nodes involved.
    
    ['human', 'chimp', 'galago', 'mouse', 'rat4']
    
    @param str guid_tree
        The newman style tree string.
    """
    
    regex = re.findall("[a-zA-Z1-9]+", guid_tree)
    return regex

    

def obtain_test_files_dir():
    try:
        path = os.environ["TEST_FILES"]
    except KeyError:
        print("Please set the TEST_FILES environment variable to a valid directory.")
        exit()
        
    return path
        
        
        
# Tests        
if __name__ == "__main__":
    
    obtain_test_files_dir()
    
    
    print parse_guid_tree_names("(((human chimp) galago) (mouse rat4))");
    
    find_files('fakemaf', 'd')
