"""
SleepJob

This is an example of a Job subclass; used for testing.

@author Vincent Schramer
"""
from Job import Job
from time import sleep


class SleepJob(Job):
    
    def __init__(self, *args, **kwargs):
        
        # Call parent constructor
        Job.__init__(self, *args, **kwargs)
        
        self.time = 2;
    
    def run(self):
        
        self._handle_progress_line( str(self.id) + "Start sleeping...")
        
        for i in range(int(self.time), 0, -1):
            
            self._handle_progress_line(str(self.id) + ' : ' + str(i) + "...\n")
            sleep(1)
            self._handle_error_line("simulated sleep error line after second " + str(i) + "\n")
        
        self._handle_progress_line( str(self.id) + "Done sleeping...")
            
    #
    # time property
    #
    @property
    def time(self):
        
        return self._time
    
    @time.setter
    def time(self, value):
        self._time = float(value)
