"""
RoastJob.py

Contains the RoastJob class.

@author Vincent Schramer
"""
from Job import Job
from Wrappers.RoastController import RoastController
import os
import sys

class RoastJob(Job):
    """
    This is an implementation of a job which manages a roast invokation.
    It has a roast controller as a public data member, to where 
    parameters can be passed to.
    """
    def __init__(self, *args, **kwargs):
        
        # Call parent constructor
        Job.__init__(self, *args, **kwargs)
        
        self.roast = RoastController()
        
        # Subscribe to when the RoastController emitts a line 
        # from stdout or stderr.
        self.roast.on_progress_line.append(self._handle_progress_line)
        self.roast.on_error_line.append(self._handle_error_line)
        
        
    def run(self):
        
        """
        Logic to begin the roast job.
        
        Will catch all exceptions and emitt an error line.
        """
        
        
        self._handle_progress_line("RoastJob: Invoking roast..." + os.linesep)
        
        
        try:
            
            self.roast.run_job()
        
        except:
            
            # Obtain exception object.
            e = sys.exc_info()[1]
            
            self._handle_error_line("RoastJob: "
                + str(e) + os.linesep)
            
            self._handle_progress_line("RoastJob: fail" + os.linesep)
            
            return
            
        
        self._handle_progress_line("RoastJob: complete." + os.linesep)


import os

def roast_test_job():
    
    from Wrappers.RoastController import apply_RoastController_test_config
    
    test_files_dir = os.environ["TEST_FILES"]
    
    job = RoastJob()
    
    
    def x(x):
        print x,
    
    job.on_progress_text_update.append(x)
    
    apply_RoastController_test_config(job.roast)
    
    return job

if __name__ == "__main__":
    
    job = roast_test_job()
    
    job.run()
