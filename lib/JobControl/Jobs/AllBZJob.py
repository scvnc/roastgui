"""
AllBZJob.py

Contains the AllBZJob class.

@author Vincent Schramer
"""

from Job import Job
import os
import sys

from Wrappers.AllBZController import AllBZController

class AllBZJob(Job):
    """
    Class inherited from Job.
    This job essentially contains a data member which holds an
    AllBZController, which manages the parameters and invocation 
    of all_bz.
    
    It overrides the run method, which is part of the Job interface. 
    The JobManager can invoke the run method when the Job is desired 
    to run.
    """
    
    def __init__(self, *args, **kwargs):
        
        # Call parent constructor
        Job.__init__(self, *args, **kwargs)
        
        self.all_bz = AllBZController()
        
        
        # Subscribe to when the all_bz emitts a line 
        # from stdout or stderr.
        self.all_bz.on_progress_line.append(self._handle_progress_line)
        self.all_bz.on_error_line.append(self._handle_error_line)
        
    def run(self):
        
        """
        Logic to begin the all_bz job.
        
        Will catch all exceptions and emitt an error line.
        """
        
        self._handle_progress_line("AllBZJob: Invoking all_bz..." + os.linesep)
        
        
        try:
            self.all_bz.run_job()
            
        except:
            
            # Obtain exception object.
            e = sys.exc_info()[1]
            
            self._handle_error_line("AllBZJob: "
                + str(e) + os.linesep)
            
            self._handle_progress_line("AllBZJob: fail" + os.linesep)
            
            return
                
        
        self._handle_progress_line("AllBZJob: complete." + os.linesep)




def all_bz_test_job():
    
    test_files_dir = os.environ["TEST_FILES"]
    
    job = AllBZJob()
    
    # Add sequences
    job.all_bz.add_sequence_file("human", test_files_dir + '/fasta/human.fa')
    job.all_bz.add_sequence_file("galago", test_files_dir + '/fasta/galago.fa')
    job.all_bz.add_sequence_file("chimp", test_files_dir + '/fasta/chimp.fa')
    job.all_bz.add_sequence_file("mouse", test_files_dir + '/fasta/mouse.fa')
    job.all_bz.add_sequence_file("rat", test_files_dir + '/fasta/rat.fa')
    job.all_bz.add_sequence_file("rat4", test_files_dir + '/fasta/rat.fa')
    
    # Remove other rat
    job.all_bz.remove_sequence_file("rat")
    
    job.all_bz.guid_tree = "(((human chimp) galago) (mouse rat4))"
    
    job.all_bz.reference_species = "human"
    
    job.all_bz.output_dir = test_files_dir + '/maf'
    
    job.all_bz.specfile = test_files_dir + '/fasta/specfile'
    
    def x(x):
        print x,
    
    
    job.all_bz.on_progress_line.append(x)
    job.all_bz.on_error_line.append(x)
    
    return job

if __name__ == "__main__":
    
    job = all_bz_test_job()
    
    job.run()
