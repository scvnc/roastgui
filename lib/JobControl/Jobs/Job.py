"""
Job.py

Contains the Job class 

It overrides the run method, which is part of the Job interface. 
The JobManager can invoke the run method when the Job is desired to run.

@author Vincent Schramer
"""
from uuid import uuid1
from datetime import datetime
from threading import Lock

class Job(object):
    """
    The Job class behaves as an abstract interface class
    for other Job types.  Jobs are intended to be units of work that can
    be submitted to the JobManager class.
    """

    # These are some defined enumerable statuses that can be used to
    # describe the state of the job.  For example, in the QtGUI,
    # when viewing a job, it polls this enumerable and changes the 
    # progress bar to depict it's status.
    status_enums = { 
        "FAILED"       : -1, # failed to create.
        "CREATED"      : 0,  # was successfully created.
        "QUEUED"       : 1,  # is queued to be executed.
        "RUNNING"      : 2,  # is currently executing
        "RUNTIME_ERROR": 3,  # failed while executing.
        "COMPLETE"     : 4   # is complete.
    }
    
    def __init__(self):
        
        # We can give the job a unique identifier using the uuiud module
        self.id = uuid1()
        
        # Scheduling / Time
        self.create_time = datetime.now()
        
        # Perhaps in the future we can schedule jobs.
        self.start_time  = datetime.now()
        
        # This is a default name assignment to the job.
        self.name = 'Job @ ' + str(self.create_time)
        
        
        # The job may emitt lines of text indicating progress
        self._progress_text = ""
        
        # This is a mutex object for making _progress_text safe
        # It was implemented here because there were issues
        self._progress_text_lock = Lock()
        
        # Similarly, the job may emitt lines of text indicating errors.
        self._error_text = ""
        
        # Set the initial status of the job.
        self.status = 0
        
        # Event subscriptions
        self.on_progress_text_update = []
        self.on_error_text_update = []
    
    
    # Readonly property, progress_text
    @property
    def progress_text(self):
        return self._progress_text
        
    @progress_text.setter
    def progress_text(self, x):
        return
    
        
    # Readonly property, error_text
    @property
    def error_text(self):
        return self._error_text
        
    @error_text.setter
    def error_text(self, x):
        return
    
    
    #
    # # #
    # Output line handlers
    # These methods handle lines from the job output and send them
    # to the subscribed methods for further handling.
    
    def _handle_progress_line(self, line):
        
        # Append the line
        self._progress_text_lock.acquire()
        self._progress_text += line
        self._progress_text_lock.release()
        
        # Notify subscribers
        for f in self.on_progress_text_update:
            f(line)
            
            
    def _handle_error_line(self, line):
        
        # Append the line
        self._error_text += line
        
        # Notify subscribers
        for f in self.on_error_text_update:
            f(line)
            
    #
    # # #
    # Meta functions.
    
    def __del__(self):
        pass
    
    def run(self):
        pass
    
    def __str__(self):
        return self.name
    
    def __hash__(self):
        return int(self.id)
