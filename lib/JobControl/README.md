# JobControl Module

Contains submodules that deal with the management and submission of Jobs.

### Submodules  
* JobManager
> 	Manages Job objects, keeps track of their execution and other metadata
* Jobs  - contains the base Job class and inherited Job types.