"""
The JobManager accepts Job objects and runs them. This was developed
in mind when a Web interface was requested and the application has
shifted towards it's use.

TODO: queue style management.


@author Vincent Schramer
"""

from time import sleep
from Jobs.Job import Job
from Jobs.SleepJob import SleepJob
from datetime import datetime



from threading import Thread



class JobManager():
    """
    Implementation of a construct which manages jobs.
    Currently just runs them without any queueing.
    """
    
    def __init__(self):
        
        self._job_set = {}
        
        # Event Subscriptions
        self.on_job_done = []
        self.on_queue_update = []
        self.on_job_added = []
        
    def add_job(self, job):
        
        self._job_set[str(job.id)] = job
        
        self._signal_queue_update(job)
        
        # Start the job immediately
        # Perhaps a queue can be implemented later.
        self._start_job(job)
            
    def remove_job(self, job):
        
        del self._job_set[job.id]
    
    def retrieve_job(self, jobid):
        
        if jobid in self._job_set:
            return self._job_set[jobid]
        
        else:
            return None
        
        
    def _start_job(self, job):
        """
        Starts the specified job on it's own thread.
        
        TODO: Perhaps we can attach the thread as a property of the job
        to facilitate killing it later?
        """
        thread = JobManagerThread(job)
        
        # Have the thread let us know that it's done
        thread.on_thread_done.append(self._signal_job_done)
        
        thread.start()
        
        job.status = Job.status_enums["RUNNING"]
    
    #
    # # # #
    # Events
    
    
    
    # Notifies when the queue changes: callback(job)
    
    def _signal_queue_update(self, job):
        """ Invokes all callbacks in the subscribers list. """
        for delegate in self.on_queue_update:
            delegate(job)
    
    
    
    def _signal_job_added(self, job):
        
        for delegate in self.on_job_added:
            delegate(job)
    
        self._signal_queue_update()
    
    
    
    def _signal_job_done(self, job):
        
        job.status = Job.status_enums["COMPLETE"]
        
        for delegate in self.on_job_done:
            delegate(job)
            
    


        
class JobManagerThread(Thread):
    """ 
    If this application is to optionally use the QT threads instead
    of the Python threads, then this wrapper class may be nessessary.
    
    UPDATE: currently the intermixing of thread types is handled by 
    using a thread-safe queue from the Python Queue module.
    """
    
    
    def __init__(self, job):
        Thread.__init__(self)
        
        self.job = job
        
        self.on_thread_done = []
        
    
    def run(self):
        """ 
        The method that is invoked on a separate thread.
             
        @signal thread_done
            Upon completion of the job
            
        """
        # run the job
        self.job.run()
        
        # When the job is complete, signal all the subscribers.
        self._signal_thread_done(self.job)
    

    
    def _signal_thread_done(self, job):
        for delegate in self.on_thread_done:
            delegate(job)
    


if __name__ == "__main__":
    
    from JobControl.Jobs.AllBZJob import all_bz_test_job
    from JobControl.Jobs.RoastJob import roast_test_job
    
    job_manager = JobManager()
    
    def notify_queue(job):
        print "Callback from queue_update event. " + job.name 
    
    def job_done(job):
        print job.name + ' is done.'
    
    job_manager.on_queue_update.append(notify_queue)
    job_manager.on_job_done.append(job_done)
    
    #job_manager.add_job(all_bz_test_job())
    #job_manager.add_job(roast_test_job())
    
    print ("Adding 10 jobs")
    
    for i in range(0,10):
        job = SleepJob()
        job.name = "job" + str(i)
        job_manager.add_job(job)
    
    
    for i in job_manager._job_set:
        print job_manager._job_set[i].name
        
    
