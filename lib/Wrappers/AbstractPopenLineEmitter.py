"""
AbstractPopenLineEmitter.py

I noticed that I was beginning to share some of code between 
AllBZController and RoastController. I'm going to have both of them
inherit from this class with the shared methods and other data.

I have not come up with a better name yet.

@author Vincent Schramer
"""

from subprocess import Popen, PIPE
from Utilities import LineReaderThread

class AbstractPopenLineEmitter(object):
    """
    This holds some shared line emission related methods and 
    data members
    """
    
    def __init__(self):
        
        # Subscriber list for when there is a new line.
        self.on_progress_line = []
        
        # Subscriber list for when there is a new error line.
        self.on_error_line = []
        
    
    def _handle_lines_while_running(self, popen):
        """
        Given a Popen object, this will continuously poll stdout
        and stderr for lines. It will cause this class to notify
        all the subscribers of on_progress_line and on_error_line
        with the resulting line.
        
        This is used so that lines can bubble up to the UI for handling.
        
        @param Popen popen
            Popen object, one that was created by the subprocess module.
            It expects that stdout, stderr are set to the PIPE enum.
            
            popen = Popen(cmd, stdout=PIPE, stderr=PIPE)
        
        @return int
            Returns the status code of the popen.poll() call when complete.
        """
        

        
        out_reader = LineReaderThread(popen, popen.stdout, 
                self._emitt_progress_line)
        
        
        err_reader = LineReaderThread(popen, popen.stderr, 
                self._emitt_error_line)
                
        out_reader.start()
        err_reader.start()
        
        
        # Wait for the readers to stop.
        err_reader.join()
        out_reader.join()
        
        return popen.poll()
        
    #
    # # #
    # Line handlers
    # These methods handle lines from the roast output and send them
    # to the subscribed methods for further handling.
    
    def _emitt_progress_line(self, line):
        for f in self.on_progress_line:
            f(line)
            
    def _emitt_error_line(self, line):
        for f in self.on_error_line:
            f(line)
