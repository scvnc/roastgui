"""
Workspace.py

This is a class which manages a temporary directory for use when running
a job.  It was needed because the roast toolchain expects sequence files
to be relative to the current working directory. Also because the roast
toolchain expects files to have a particular naming convention.

This class utilizes the tempfile module, which automatically handles
the creation of a temporary directory.  It will automatically handle the
destruction of the temporary directory IF you utilize this class using
Python's `with` statement.  Otherwise it's up to you to call create and
destroy.

TODO: convert this class to use python-style properties instead of 
a series of getter/setters.

@author Vincent Schramer

"""
import os
import tempfile
from shutil import rmtree
from os import path
from subprocess import Popen
from time import time, sleep

class Workspace():
    """
    This is the workspace class.  See the file docstring.
    """
    
    def __init__(self):
        
        # To maintiain a list of files to delete upon destruction.
        self._unlink_stack = []
    
    
    def __enter__(self):
        """
        This method allows this class to be utilize the "with" statement
        that Python provides. 
        """
        self.create()
        return self
        
    
    def create(self):
        """
        Creates the workspace.
        """
        self._workspace_dir = self._create_temp_dir()
        
        # I do not remember what this is for and it may be depricated.
        # I can imagine that it was from a point in time before I 
        # decoupled the classes further and this is when Workspace
        # handled external invokation (subprocess.Popen)
        self._PATH=""
        
    def get_path(self):
        return self._PATH
        
    
    def get_workspace_dir(self):
        return self._workspace_dir
        
    
    def destroy(self):
        """
        Cleans up the temporary workspace.
        """
        self._empty_unlink_stack()
        
    
    def set_directory(self, d):
        """
        Potentially set the directory to a new location.
        """
        if not os.path.isdir(d):
            raise "isdir fail "+d
            
        self._workspace_dir = d

    
    def create_species_symlinks(self, species):
        """
        I beleive this is depricated, back when Workspace had a bad case
        of class cohesion and needed to be broken up. The logic of 
        creating a series of symlinks for species files is now done
        outside of this class.  See create_symlink(..)
        """
        
        for k, v in species.items():
            self.create_symlink(path.abspath(v), k)
            
        
    
    # Creates symlink relative to workspace dir.
    def create_symlink(self, link, name):
        """
        This creates a symlink relative to the workspace directory.
        
        NOTE: ALTERNATIVE IMPLEMENTATION NEEDED IF OS IS WINDOWS.
        """
        
        link = path.abspath(link)
        name = self._workspace_dir+'/'+name
        
        os.symlink(link, name);
        
        self._unlink_stack.append(name);
        
        
    def _create_temp_dir(self):
        """
        This creates a temporary directory with the tempfile module.
        
        @returns str
            Pathname to the temporary directory.
        """
        
        tmpdir = tempfile.mkdtemp()
        
        self._unlink_stack.append(tmpdir)
        
        return tmpdir
        
        
            
    
    def _empty_unlink_stack(self):
        
        """
        This deletes everything that was pushed on to the _unlink_stack
        data member.  Cleanup process.
        """
        
        # While the stack isn't empty.    
        while len(self._unlink_stack) != 0:
            
            # Pop the path off the stock
            node = self._unlink_stack.pop()
            
            # Debugging information
            print 'rm', node
            
            # Recursively delete the path if it's a directory.
            if path.isdir(node):
                rmtree(node)
            
            # otherwise simply delete the path.
            else:
                os.unlink(node)
        
        

    
    def __exit__(self, type, value, traceback):
        """
        Upon exit (when using the with statement,) clean up the 
        temporary workspace.
        """
        self.destroy()



if __name__ == "__main__":

    pass

