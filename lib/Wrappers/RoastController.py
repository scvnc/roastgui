"""
RoastController Module

Primarilly contains the RoastController class and a test routine.

The RoastController is Python interface to managing and invoking a 
roast job.  Many of the command line arguments are available as 
properties of the class.

Ideally this is where validation should occur and an exception should
be thrown for an error handler to manage. This part is not yet 
implemented.

@author Vincent Schramer
"""



from Wrappers.Workspace import Workspace
from Wrappers.AbstractPopenLineEmitter import AbstractPopenLineEmitter

from Utilities import parse_guid_tree_names
from subprocess import Popen, PIPE, STDOUT
from time import sleep

import os
import shutil



class RoastController(AbstractPopenLineEmitter):
    
    def __init__(self):
        """
        RoastController initializer (constructor)
        
        Initializes properties for the class
        """
        AbstractPopenLineEmitter.__init__(self)
        
        self._workspace = Workspace()
        
        self._sequences = dict()
        
        self.guid_tree         = ""
        self.reference_species = ""
        self.output_file       = ""
        
        self.min_len = 1
        self.dpr = 30
        self.output_file = ""
        self.strand = "+"
    
    
    def __del__(self):
        """
        RoastController delete method (destructor)
        """
        # Remove all the temporary files from the workspace.
        self._workspace.destroy()
        
    
    
    
    def run_job(self):
        """
        Once all the required parameters are available, this will 
        invoke the all_bz process.
        """
        self._prepare_workspace()
        
        
        # Prepare the command.
        cmd = ['roast', 
               self.strand, # Strand
               'R=' + str(self.dpr), # Dynamic programming radius
               'M=' + str(self.min_len), # minimum block length of output.
               'E='+self.reference_species, 
               self.guid_tree]
        
        # Append all the maf file names.
        for maf in self._sequences:
            maf_file = os.path.basename(maf)
            cmd.append(maf_file)
        
        # Default temporary output file name.
        # This is not the real output file name.
        cmd.append("roast.maf")
        
        # Obtain the current working directory from the temporary
        # directory the Workspace is managing.
        cwd = self._workspace.get_workspace_dir()
        
        self._assert_required_sequences();
        # TODO: more preparedness checks.
        
        # Invoke the command
        cmd = Popen(cmd, cwd=cwd, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        
        self._handle_lines_while_running(cmd)
            
        
        # Post-job cleanup.
        self._move_output()
        
        # Refresh workspace.
        self._workspace.destroy()
    
    
    
    def _move_output(self):
        """
        This should be invoked upon completion of the job. It moves
        the output file to where it should really be.
        """
        
        # Obtain the workspace directory
        src_dir = self._workspace.get_workspace_dir()
        
        # Obtain where the maf files ought to go.
        dest_file = self.output_file
            
        src = src_dir+'/'+"roast.maf"

        try:
            shutil.move(src, dest_file)
        except IOError:
            self._emitt_error_line("RoastController: No output produced!")
            pass
            
    
    def _check_file_readable(self, f):
        """
        Checks if a file can be read.
        
        @param str f
            The pathname to check.
        """
        
        try:
            # For now just try to open it in read mode.
            with open(f, 'r') as file:
                pass
        
        except IOError:
            self._emitt_error_line("RoastController: Could not read " + f)
            pass
        
    
    def _prepare_workspace(self):
        """
        This should prepare the workspace folder for
        invocation of roast.
        """
        
        self._workspace.create()  
        
        # Make a symlink for each sequence file
        # workspace_dir/species_id -> /full/path/species_xyz.fa
        for species, path in self._sequences.iteritems():
            self._check_file_readable(path)
            self._workspace.create_symlink(path, species)

      
        
    def _assert_required_sequences(self):
        """
        This should assert that the required sequence files (based on 
        the guid tree) are available.
        """
        # Not implemented.. Need determiner based on GUID tree.
        species = parse_guid_tree_names(self.guid_tree)


    #
    # # #
    # Access Methods
    
    
    def add_sequence_file(self, full_path):
        """ 
        This adds a species to the internal collection.
        
        @param str full_path
            The full path to the sequence file.
        """
        self._sequences[os.path.basename(full_path)] = full_path
        
        
    def remove_sequence_file(self, path):
        """ 
        This removes a species from the internal collection.
        It will not complain if the species isn't already in the collection.
            
            @param str species_id
                The species identifier.
        """
        try:
            self._sequences.pop(path)
            
        except KeyError:
            # Ignore if the key does not exist.
            pass
            
    
    def clear_sequence_files(self):
        self._sequences.clear()
    
    
    
    
    # reference_species property
    @property
    def reference_species(self):
        
        # TODO: Validation that the provided reference species is
        # a key inside the sequence collection.
        
        return self._reference_species
        
    @reference_species.setter
    def reference_species(self, ref):
        self._reference_species = ref
    
    
    # maf_files property
    @property
    def maf_files(self):
        
        lst = []
        
        for index in self._sequences:
            lst.append(self._sequences[index])
            
        return lst
    
    @maf_files.setter
    def maf_files(self, ref):
        # Read-only for now
        # TODO: Exception?
        return
#
# # #
# Tests
#
    
def apply_RoastController_test_config(roast):
    
    from Utilities import obtain_test_files_dir
    
    # Contains some test files.
    test_files_dir = obtain_test_files_dir()
    
    files = ['chimp.galago.sing.maf', 'chimp.mouse.sing.maf',
             'galago.mouse.sing.maf', 'galago.rat4.sing.maf',
             'human.chimp.sing.maf',
             'human.galago.sing.maf', 'human.mouse.sing.maf',
             'human.rat4.sing.maf', 'mouse.rat4.sing.maf'];
             
    for maf in files:
        maf_folder = os.path.join(test_files_dir, 'maf', maf)
        roast.add_sequence_file(maf_folder)
        
    roast.guid_tree = "(((human chimp) galago) (mouse rat4))"
    
    roast.reference_species = "human"
    
    roast.output_file = os.path.join(test_files_dir, 'roast_out.maf')
    
    
    
    
if __name__ == "__main__":
    
    rc = RoastController()
    
    
    def x(x):
        print x,
        
    rc.on_progress_line.append(x)
    rc.on_error_line.append(x)
    
    apply_RoastController_test_config(rc)
    
    rc.run_job()
