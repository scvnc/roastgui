"""
AllBZController Module

Primarilly contains the AllBZController class and a test routine.

The AllBZController is Python interface to managing and invoking an
AllBZ job. Many of the command line arguments are available as 
properties of the class.

Ideally this is where validation should occur and an exception should
be thrown for an error handler to manage. This part is not yet 
implemented.

@author Vincent Schramer

"""

from Workspace import Workspace
from Utilities import parse_guid_tree_names
from subprocess import Popen, PIPE
from Wrappers.AbstractPopenLineEmitter import AbstractPopenLineEmitter


import os
import shutil



class AllBZController(AbstractPopenLineEmitter):
    
    
    def __init__(self):
        """
        AllBZController initializer (constructor)
        
        Initializes properties for the class
        """
        
        # Call parent's constructor.
        AbstractPopenLineEmitter.__init__(self)
        
        self._workspace = Workspace()
        
        self._sequences = dict()
        
        self._guid_tree  = "";
        self._reference_species = "";
        self._output_dir = "";
        self._specfile   = "";
        
        
    def __del__(self):
        """
        RoastController delete method (destructor)
        """
        # Remove all the temporary files from the workspace.
        self._workspace.destroy()
        
    
    
    
    def run_job(self):
        """
        This will prepare and invoke the all_bz command.
        """
        self._prepare_workspace()
        
        
        # Prepare the command.
        cmd = ['all_bz', '+', 'F='+self._reference_species, self._guid_tree]
        
        
        if self._specfile != '':
            cmd.append('specfile')
        
        cwd = self._workspace.get_workspace_dir()
        
        
        self._assert_required_sequences();
        # TODO: more preparedness checks.
        
        # Invoke the command.
        cmd = Popen(cmd, cwd=cwd, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        
                
        self._handle_lines_while_running(cmd)
            
        
        # Post-job cleanup.
        self._move_output()
        
        # Refresh workspace.
        self._workspace.destroy()
    
    

    
    
    def _move_output(self):
        """
        After the AllBZ command is complete, the output needs to be
        moved to the real output location.
        """
        
        # Obtain the workspace directory
        src_dir = self._workspace.get_workspace_dir()
        
        # Obtain where the maf files ought to go.
        dest_dir = self.output_dir
        
        files = os.listdir(src_dir)
        
        # We only want MAF files.
        files = filter(lambda x: ".maf" in x, files)
        
        self._emitt_progress_line("AllBZController: moving output..." + os.linesep) 
        
        # After finding the full path, move each file over.
        for f in files:
            
            fullpath = src_dir+'/'+f
            basename = f
            dirname = os.path.dirname(fullpath)
            
            src = dirname+'/'+basename
            dest = dest_dir+'/'+basename
            
            #print ("Debug: Moving " + src + " -> " + dest)
            
            # Perform movement
            shutil.move(src, dest)
            
            
            self._emitt_progress_line("Output file: " + basename + os.linesep) 
        
        
            
    
    def _check_file_readable(self, f):
        """
        Checks if a file can be read.
        
        @param str f
            The pathname to check.
        """
        
        # For now just try to open it in read mode.
        with open(f, 'r') as file:
            pass
        
    
    def _prepare_workspace(self):
        """
        This should prepare the workspace folder for
        invocation of AllBZ.
        """
        self._workspace.create()  
        
        # Make a symlink for each sequence file
        # workspace_dir/species_id -> /full/path/species_xyz.fa
        for species, path in self._sequences.iteritems():
            self._check_file_readable(path)
            self._workspace.create_symlink(path, species)
        
        # symlink the specfile if there is one defined.
        if self._specfile != '':
            self._workspace.create_symlink(self._specfile, 'specfile')
      
      
      
        
    def _assert_required_sequences(self):
        """
        This should assert that the required sequences are available
        based on the guid tree.
        """        
        
        
        species = parse_guid_tree_names(self._guid_tree)
        
        for s in species:
            
            if s not in self._sequences:
                raise RuntimeError("Need sequence file for " + s)
        


    #
    # # #
    # Access Methods
    
    
    def add_sequence_file(self, species_id, full_path):
        """ 
        This adds a species to the internal collection.
        
        @param str species_id
            The species identifier (example: mouse, rat, cow2012)
        
        @param str full_path
            The full path to the sequence file.
        """
        self._sequences[species_id] = full_path
        
        
    def remove_sequence_file(self, species_id):
        """ 
        This removes a species from the internal collection.
        It will not complain if the species isn't already in the collection.
            
            @param str species_id
                The species identifier.
        """
        try:
            self._sequences.pop(species_id)
            
        except KeyError:
            # Ignore if the key does not exist.
            pass
            
    
    def clear_sequence_files(self):
        self._sequences.clear()
        
    
    
    
    # guid_tree property
    @property
    def guid_tree(self):
        return self._guid_tree
    
    @guid_tree.setter
    def guid_tree(self, guid_tree):
        self._guid_tree = guid_tree
    
    
    # reference_species property
    @property
    def reference_species(self):
        return self._reference_species

    
    @reference_species.setter
    def reference_species(self, ref_species):
        
        # TODO: Validation that the provided reference species is
        # a key inside the sequence collection.
        
        self._reference_species = ref_species
    
    
    # output_dir property
    @property
    def output_dir(self):
        return self._output_dir
        
    @output_dir.setter
    def output_dir(self, full_dir_path):
        self._output_dir = full_dir_path
    
    
    # specfile property
    @property
    def specfile(self):
        return self._specfile
    
    @specfile.setter
    def specfile(self, full_specfile_path):
        self._specfile = full_specfile_path
        
#
# # #
# Tests
#

if __name__ == "__main__":
    
    from Utilities import obtain_test_files_dir
    
    
    test_files_path = obtain_test_files_dir()
    
    
    all_bz = AllBZController()
    
    # Add sequences
    all_bz.add_sequence_file("human", os.path.join(test_files_path, 'fasta/human.fa'))
    all_bz.add_sequence_file("galago", os.path.join(test_files_path, 'fasta/galago.fa'))
    all_bz.add_sequence_file("chimp", os.path.join(test_files_path, 'fasta/chimp.fa'))
    all_bz.add_sequence_file("mouse", os.path.join(test_files_path, 'fasta/mouse.fa'))
    all_bz.add_sequence_file("rat", os.path.join(test_files_path, 'fasta/rat.fa'))
    all_bz.add_sequence_file("rat4", os.path.join(test_files_path, 'fasta/rat.fa'))
    
    # Mistype
    all_bz.remove_sequence_file("rar")
    
    # Remove other rat
    all_bz.remove_sequence_file("rat")
    
    all_bz.guid_tree = "(((human chimp) galago) (mouse rat4))"
    
    all_bz.reference_species = "human"
    
    all_bz.output_dir = os.path.join(test_files_path, 'maf')
    
    all_bz.specfile = os.path.join(test_files_path, 'fasta/specfile')
    
    def x(x):
        print x,
        
    all_bz.on_progress_line.append(x)
    all_bz.on_error_line.append(x)
    
    all_bz.run_job()
