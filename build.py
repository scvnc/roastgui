#!/usr/bin/env python

import shutil
import os
import urllib
import tarfile
from subprocess import check_call, CalledProcessError
from time import sleep

#
# # # # # # # #
# Build functions. 

def build_ui_py_files():
    """
    Compiles all of the Qt4-Designer files in ./QtGUI/designer/ui
    to their python classes for inheritance.
    """
    
    # Attempt to import PyQt4
    try:
        from PyQt4 import uic
        
    except ImportError:
        print (__file__ + ": NOTICE: PyQt4 not found, UI will not be available.")
        return
    
    ui_files_path = './QtGUI/designer/ui'
    
    uic.compileUiDir(ui_files_path)
    
    move_py_files(ui_files_path, './QtGUI/designer')

def build_roast(roast_path):
    """
    @param str roast_path
        Path to the roast directory.
    """
    
    makefile = os.path.join(roast_path, 'Makefile')
    
    # If it doesn't seem like the roast source is there,
    # have git initialize the submodules of this repo.
    if not os.path.exists(makefile):
        
        print(__file__ + ": obtaining roast toolchain.")
        git_initialize_submodules()
    
    
    # Invoke roast's makefile.
    print(__file__ + ": building roast toolchain.")
    make_roast_toolchain(roast_path)
    
def obtain_test_files():
    """
    Obtain a set of test files.
    """
    
    url = 'https://bitbucket.org/scvnc/roastgui/downloads/test_files.tar.gz'
    filename = './test_files.tar.gz'
    
    if not os.path.exists(filename):
        
        print ("Downloading " + url)
        
        urllib.urlretrieve(url, filename)
        
    
    test_files = tarfile.open(filename)
    
    print ("Extracting " + filename)
    
    test_files.extractall()
    
    
def check_for_blastz():
    
    print("build.py: Checking for blastz...")
    
    try:
        # invoke blastz, hide output by piping
        check_call(["blastz", '--version'])
    
    except OSError, e:
        
        if e.errno == 2:
            
            print("build.py: NOTICE: blastz could not be found, please refer to the README")
            
            sleep(2)
            
        else:
            raise e
    
    except CalledProcessError:
        pass

    
#
# # # # # # # # # # #
# Utility functions
#

def git_initialize_submodules():
    
    try:   
    
        # Initialize submodules
        check_call(["git", "submodule", "init"])
        
        print ("build.py: You may need to provide your Bitbucket credentials when obtaining roast.")
        
        # Pull the submodles
        check_call(["git", "submodule", "update"])
        
    except OSError, e:
        
        # Handle case where git is not found.
        if e.errno == 2:
            
            print("Error: could not invoke git; is it installed?")
            exit(1)
            
        else:
            raise e

def make_roast_toolchain(roast_path):
    
    try:
        check_call(["make", "-C", roast_path])
        
    except OSError, e:
        
        # Handle case where make is not found.
        if e.errno == 2:
            
            print("Error: could not invoke make; is it on your PATH?")
            exit(1)
            
        else:
            raise e
    

def move_py_files(src_dir, dest_dir):
    """
    Moves all py files from src_dir to dest_dir
    
    @param str src_dir
        Python files will come from here
    
    @param str dest_dir
        Python files will be placed in here
    """
    
    py_files = find_py_files(src_dir)
    
    move_files(py_files, dest_dir)
    
    
def move_files(file_list, dest):
    """
    Calls shutil.move on multiple files.
    
    @param iteratable file_list
        A iteratabile collection of file paths to move.
        
    @param dest
        The destination folder.
    """
    
    for f in file_list:
        shutil.move(f, dest + os.path.sep + os.path.basename(f))

def find_py_files(path):
    """
    Given a directory, this returns a list of 
    files that contain .py in it's filename.
    
    @param string path
        path to search
    """
    
    ls = os.listdir(path)
    
    pyfiles = filter(lambda x: x.find('.py') != -1, ls)
    
    # Prepend the search path behind the file name.
    full_pyfiles = []
    
    for f in pyfiles:
        full_pyfiles.append(path + os.path.sep + f)
    
    return full_pyfiles
    
def get_response(question):
    
    print(question), '(y/n):', 
    
    response = raw_input()
    response = response.lower()[0]
    
    if response == 'y':
        return True
    else:
        return False
#
# # # # 
# Script 
    
if __name__ == "__main__":
    
    print(__file__ + ": building user interface files...")
    build_ui_py_files()
    
    check_for_blastz()
    
    if get_response("Would you like to obtain and build the roast toolset?"):
        build_roast('./lib/roast/')
    
    if get_response("Would you like to download test sequence files?"):
        obtain_test_files()
