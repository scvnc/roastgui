#!/usr/bin/env python

import cherrypy
import json
import os

import sys

# Path hack for starting when cwd is in the same directory of this file.
sys.path.append('../lib')

from JobControl.JobManager import JobManager

from JobControl.Jobs.RoastJob import RoastJob
from JobControl.Jobs.SleepJob import SleepJob

from JobSerializers import JSONRoastJobHandler



class Jobs(object):
    
    pass

class JobManagerRes(object):
    
    exposed = True
    
    def __init__(self, job_manager=JobManager()):
        
        self.job_manager = job_manager
        self.count = 0
        
        # Q: When recieving a job, how should it be instantiated as a job?
        # A: This dictionary associates a job class with a class which
        # has a serialize and deserialize method.
        self.job_handlers = { 
                                "RoastJob"  : JSONRoastJobHandler(),
                                "AllBZJob"  : JSONRoastJobHandler()
                            }
        
        # DEBUG
        job_manager.add_job(SleepJob())
    
    def GET(self, id = None):
        
        
        
        if id == None:
            return "Not implemented: listing of all jobs."
        
        
        # Obtain the job.
        job = self.job_manager.retrieve_job(id)
        
        if job == None:
            raise cherrypy.HTTPError(status=404, message="Job not found.")
        
        
        
        # Retrieve the job handler class
        job_handler = self.job_handlers[job.__class__.__name__]
        
        
        request_type = cherrypy.lib.cptools.accept(media=("text/html","application/json"))
        
        # HTML REQUEST
        if request_type == "text/html":
            json_string = job_handler.serialize(job)
            
            
            # Temporarilly load the json string back into a dict.
            temp_dict = json.loads(json_string)
            
            # Convert back to string, but formatted nicely.
            json_string = json.dumps(temp_dict, sort_keys=True, 
                                     indent=4, separators=(',', ': '))
            
            output_file = """
            <h2> Job report for ID: """ + str(job.id) + """</h2>
            
            <h4> Minimally formatted job properties </h4>
            <pre> """ + json_string + """</pre>
            
            <h4> status enum definition </h4>
            <pre>
                    "FAILED"       : -1, # failed to create.
                    "CREATED"      : 0,  # was successfully created.
                    "QUEUED"       : 1,  # is queued to be executed.
                    "RUNNING"      : 2,  # is currently executing
                    "RUNTIME_ERROR": 3,  # failed while executing.
                    "COMPLETE"     : 4   # is complete
            </pre>
            
            <h4> link to output file (if complete & type: RoastJob) </h4>
            <a href='./""" + temp_dict['job']['output_file'] + """'>output.maf</a>
            """
            return output_file
        
        # JSON REQUEST
        elif request_type == "application/json":
            
            # Serialize the job with the serializer class.
            json_string = job_handler.serialize(job)
            
            # Return the string
            return json_string
    
    
    def POST(self, doop=None):
        """
        Currently accepts a job via JSON
        """
        body = cherrypy.request.body.read()

        
        # print cherrypy.request.headers.get("Content-Type")
        
        params = json.loads(body)
        
        # Exit if the job class is unknown.
        job_class = params['job_class']
        
        if job_class not in self.job_handlers:
            cherrypy.response.status = 400
            return 'Unknown job type "' + params['job_class'] + '."'
            #-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
        
        # Retrieve job handler instance
        job_handler = self.job_handlers[job_class]
        
        # Generate the job
        # TODO: Parameter validation?
        job =  job_handler.deserialize(params["job"])
        
        self.job_manager.add_job(job)
        
        
        res_obj = dict()
        
        
        res_obj["id"]     = str(job.id)
        res_obj["status"] = job.status
        
        return json.dumps(res_obj)
        
        
"""

{
    job_class: "AllBZJob",
    job_params: { }
}

"""

class root(object):
    
    exposed = True
    

    
    jobmgr = JobManagerRes()
    
    def GET(self):
        raise cherrypy.HTTPRedirect("/client/")

    
    




# App behavior configuration.
config = {'/':
    {
        'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
        'tools.staticdir.root' : os.path.dirname(os.path.abspath(__file__)),
        'tools.staticdir.dir' : "client",
        'tools.staticdir.index' : "index.html"
    },
    '/client' :
        {
            'tools.staticdir.index' : "index.html",
            'tools.staticdir.on' : True,
            'tools.staticdir.dir' : "client"
        },
    '/jobmgr' :
        {
            'tools.staticdir.on' : True,
            'tools.staticdir.dir' : "jobs"
        }
}

cherrypy.tree.mount(root(), "/", config=config)

# Load app config.
cherrypy.config.update("app.conf")

cherrypy.engine.start()
#cherrypy.quickstart(JobControl())
