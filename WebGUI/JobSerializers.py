from JobControl.Jobs.RoastJob import RoastJob

import cherrypy

import json
import os

class JSONRoastJobHandler(object):

    def deserialize(self, job_dict):
        
        """
        Assumes structure like so:
        
        "job": {
                   "maf_files": [],
                   "reference_species": "",
                   "guid": "",
                   "dpr": 30,
                   "min_len": 1
               }
               
        TODO: User input could be shady.
        """
        
        job = RoastJob()
        
        # Add each sequence, add the sequence file to the job.
        for seq in job_dict['maf_files']:
            job.roast.add_sequence_file(seq)
        
        # Set various parameters.
        job.roast.reference_species = job_dict['reference_species']
        job.roast.guid_tree = job_dict['guid']
        job.roast.dpr = job_dict['dpr']
        job.roast.min_len = job_dict['min_len']
        
        
        # Create the job output directory.
        jobs_dir = cherrypy.config['Directories']['jobs_dir']
        
        job_dir = os.path.join(jobs_dir, str(job.id))
        
        os.mkdir(job_dir)
        
        job.roast.output_file = os.path.join(job_dir, "output.maf")
        
        
        return job


    def serialize(self, job):
        
        roast = job.roast
        
        job_dir = os.path.join(
            cherrypy.config['Directories']['jobs_dir'],
            str(job.id) );
        
        roast_params = {
            
            "maf_files" : roast.maf_files,
            "guid_tree" : roast.guid_tree,
            "reference_species" : roast.reference_species,
            "output_file" : roast.output_file,
            "min_len" : roast.min_len,
            "dpr" : roast.dpr,
            
            # Make path relative to the job directory
            # /path/to/jobs_dir/abcd-3456/file.maf => /file.maf
            "output_file" : roast.output_file.replace(job_dir, ""),
            
            "strand" : roast.strand
            
        }
        
        
        job_dict = {
        
            "id" : str(job.id),
            "name" : job.name,
            "create_time" : str(job.create_time),
            "start_time" : str(job.start_time),
            "status" : job.status,
            
            "job" : roast_params
        }
        
        json_string = json.dumps(job_dict)
        
        return json_string
