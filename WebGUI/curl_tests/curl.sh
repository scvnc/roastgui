#!/bin/bash

source config.sh

# post_json json_file resource
function post_json () {
    
    curl -d @${1} \
    ${HOSTNAME}:${PORT}${2} \
    -H "Content-Type:application/json"
    
}


post_json roastjob.json /jobmgr
