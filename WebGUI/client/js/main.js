
var output = $("#output");


$("#submit_job_btn").click(function()
{
    
    job = obtain_roast_json();
    submit_job(job);
    
});

function obtain_roast_json()
{
    
    var reference_species = $("#reference_species")[0].value;
    var guid              = $("#guid_tree")[0].value;
    var dpr               = $("#dpr")[0].value;
    var min_len           = $("#min_len")[0].value;
    
    var json_payload = 
    {
        "maf_files" : RoastJob.get_maf_file_selections(),
        "reference_species" : reference_species,
        "guid" : guid,
        "dpr" : dpr,
        "min_len" : min_len
    }
    
    return json_payload;
    
}


function submit_job(json)
{
    
    var template = {
                       'job_class' : "RoastJob",
                       'job' : json
                   };
    
    $.ajax("/jobmgr",
    {
        // Accept (and parse response as) json
        "type" : 'POST',
        "dataType" : "json",
        "contentType" : "application/json; charset=utf-8",
        "data" : JSON.stringify(template),
        
        
        "complete": function(XHR, status){},
        "success" : function(data, status){
            add_to_job_table(data.id)
            },
        "error"   : function(XHR, status, errorThrown){}

        
        
    });
    
}

function add_to_job_table(id)
{
    
    var link = '<a href="/jobmgr/' + id + '/" target="_blank">Job ID: ' + id + '</a><br/>';
    
    $("#output").append(link)
    
}

var RoastJob =
{
    
    get_maf_file_selections: function(callback)
    {
        
       var search = $('#maf_files').find('option:selected')
       
       if (search.length != 0)
       {
           
           var retr_array = []
           
           for (var i = 0; i < search.length; i++)
           {
               
               retr_array.push(search[i].innerText);
               
           }
           
           return retr_array;
       }
       
       
       else
       {
           
           return [];
           
       }

    }
    
}

