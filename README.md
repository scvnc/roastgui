roastGUI
======

roastGUI is a graphical user interface for the roast toolchain and is written in Python 2.6.

There are two separate user interfaces.

* The desktop UI utilizes [PyQt4](http://www.riverbankcomputing.com/software/pyqt/intro) library.  Qt version 4.6.
* The web interface utilizes the [CherryPy 3](http://cherrypy.org) HTTP library.
    * Currently the web interface is in a prototype stage.

### What is roast?
roast is an unreleased bioinformatics toolchain for multiple sequence alignment.  The toolchain consists of a main executable `roast` alongside several helper executables.

There is an unofficial private repository of the roast toolchain located at [https://bitbucket.org/scvnc/roast](https://bitbucket.org/scvnc/roast).


Installation
----------------

### System compatibility
The Qt user interface was developed under Linux, but due to the multi-platform nature of Python and Qt: it should also work on Mac and Windows.

The functionality of the program was developed under Linux, but also should work fine on Mac. **Compatibility with Windows is not there yet** for two reasons:

* Symlinking is used during invocation of the roast toolchain and so some minor modification will be required for use on Windows.  Please see the developer note titled: "Workspace strategy" below.
* The roast toolchain has not been modified to work with Windows yet.
    * More details on this in the later section titled _"Building binary packages"_


### Prerequisites

Python 2 of at least version 2.6 must be available on your system.

* Python 2 comes pre-installed with most modern Linux distributions.
* Python 2 can be downloaded [here](http://python.org/download/).
* This has not been tested against Python 3 and probably will not work.
* Windows and Mac users can obtain the latest version of Python 2 at the download link above.
    * Supposedly the PyQtX project suggests that Mac users download the latest version of Python 2 instead of using the included version of Python that ships with Mac.

PyQt4 must be available on your system for the Qt interface.

* Linux users are recommended to use the package manager for your distribution. PyQt4 is typically available.
* Windows users can use the binary packages available on the [PyQt website](http://www.riverbankcomputing.com/software/pyqt/download). The package named `PyQt4-*-gpl-Py2.*-Qt4.*-x64.exe` should suffice.
* Mac users have available to them the [PyQtX Project](http://sourceforge.net/projects/pyqtx/files/) which simplifies the installation of PyQt4 on Mac systems.  Please follow their instructions for installation.

CherryPy 3 must be available on your system for the web-based interface.

* This is typically available via Linux package managers or Python package managers such as easy_install.
* Alternatively, download and extract [CherryPy3](http://download.cherrypy.org/cherrypy/3.2.2/) and move the cherrypy directory  into the WebGUI folder.

BLASTZ must be available for `all_bz` functionality.

* The executable `blastz` must be available on your PATH for `all_bz` to run successfully. 
* `blastz` is freely obtainable by searching for "LASTZ" on the [PSU Comparative Genomics and Bioinformatics Website](http://www.bx.psu.edu/)
    * `blastz` has been obsoleted by LASTZ, but is backwards compatible.  Simply symlink or rename the `lastz` executable to `blastz`.
* `all_bz` is part of the roast toolchain and it generates the required Multiple Alignment Format (`*.maf`) files needed for the multiple alignment that `roast` performs.  

### Download the source code
Since this is a git repository, the easiest way to obtain the code is to use `git` on the command line.  To do this, simply run the following command:  

`git clone https://bitbucket.org/scvnc/roastgui.git`  

git will automatically download the latest version of the source code and place it in the folder "roastgui" in your current directory.  You can now continue to  the next section.

#### Updating your copy of the source code.

git makes it very easy to update your copy of the source code by invoking `git pull` inside the source code folder.  This will automatically synchronize the code with the latest version.

#### Download without git
If it is not desired to use git, then Bitbucket provides automatically generated archive files that contain the latest source code.

* [Download as tar.bz2](https://bitbucket.org/scvnc/roastgui/get/master.tar.bz2)
* [Download as tar.gz](https://bitbucket.org/scvnc/roastgui/get/master.tar.gz)
* [Download as zip](https://bitbucket.org/scvnc/roastgui/get/master.zip)

Additional downloads may be available on the Downloads tab of this Bitbucket repository.

### Setup with build.py script.
Once the source code is extracted to a folder, one can run the build.py script. The script primarily converts the Qt-Designer files into python modules but also automates obtaining the roast toolchain and obtaining test sequence files.  See the developer note titled _"What are Qt-Designer (*.ui) files?"_ for more detailed information on this process.




#### Obtaining the roast toolchain.
build.py will ask if it is desired to obtain and compile the roast toolchain (located in a private repository on BitBucket.)  Answering 'y', to this question will cause the script to download the roast repository and then invoke it's makefile.  This will only work if `git` and `make` are available on your system.  Until the repository is made public, you must type in your BitBucket credentials which have access to the repository.

If you choose not to obtain the roast toolchain by this method, you must either:

* have the roast toolchain available on your PATH  
* manually move all the roast toolchain executables to the lib/roast directory

This project relies on the roast toolchain in order to perform multiple alignment and will not work without providing the roast toolchain in some way.

#### Obtaining test sequence files.
build.py will ask if it is desired to obtain a set of sample fasta sequences for use with this tool. Answering 'y' will cause the script to download the fasta sequences and place them the test_files directory. As of 5/9/13, the download is approximately 5MB and is hosted on the "Downloads" section of this repository. 

Developer note: some modules such as (`RoastController` or `AllBZJob`) assume that this folder's path is available in the environment variable: "TEST_FILES" when running some built-in tests.


Starting roastGUI
-------------------------
Once the installation is complete, one can start the Qt Interface by invoking `roastgui.py`.

The web interface is still under development, but can be started by invoking `WebGUI/start.py`.

* Note that there are some hard-coded values within the WebGUI such as the folder where the Job Output is held, where a pool of sequences are found, and finally the HTML interface has many of the available sequences hard coded in to the page.
* The output of the console when invoking the WebGUI will note which port the server is listening on.

Developer notes
-----------------------

#### Conventions

* PyQt uses Qt naming conventions (camelCase,) but underscore_naming is done when possible for variables and functions. Class names are CamelCased.
* Private intended methods and data members are marked with an underscore (Example: self._my_private_data)
* Indentation is 4 spaces.


#### Workspace strategy
One of the strategies that roastGUI takes when invoking the roast toolchain is to make a temporary directory to be used as the current working directory when invoking roast. From there, all input files are symbolically linked to appear relative to the workspace directory. This is done because the current roast toolchain assumes that input files are relative to the current working directory.  Additionally the current roast toolchain has naming conventions required for the input files; symlinking them allows compliance to that requirement.

There is a Workspace class currently located in `lib/Wrappers/Workspace.py` which handles and manages a temporary directory.  It utilizes the [tempfile](http://docs.python.org/2/library/tempfile.html) module built in to Python.  It is imagined that there could be a case where a temporary directory made by this module may fail if typical temporary directory locations are non-writable.  Handling this case has not been investigated thoroughly.

It is also important to consider OS compatability since the Workspace class symlinks the input files.  This will be fine on modern POSIX systems that support symlinking, but will require some adjustment in the code for Windows compatibility.  One suggestion is to simply make a copy of the file instead of symlinking it (if Windows is detected.)

#### What are Qt-Designer (*.ui) files ?
The Qt framework ships with a GUI designer very similar to something like Visual Studio provides.  Qt Designer will produce XML based `*.ui` files for a window.  When using Qt with Python: one strategy is to use a provided compiler to convert the `*.ui` file into a Python class.  From there, one can write a Python class that inherits the generated class and continue from there.

#### View decoupling
The codebase is organized in such a way so that only the files related to the desktop user interface are located inside `QtGUI`.  In here is where all the definition of Windows, widgets, and logic that connects them are located.  Similarly, in the case of the prototypical web-based interface, code related only to  the web interface are located in the `WebGUI` folder.  The code that handles the invocation and management of the roast toolchain is located in a shared library directory named lib.  The `lib` path is added by appending it in the Python module search path while the application is starting: `sys.path.append("lib")`

#### Building binary packages
There is a nice tool called `pyinstaller` which turns a Python codebase into a self contained package for distribution on other systems.  Pyinstaller can be downloaded at it's website [here](http://pyinstaller.org)

For example, one could set-up roastGUI their Mac and then apply `pyinstaller` to their set-up.  The result would be a Mac package that could be distributed to machines which do not have the the prerequisites such as PyQt4 and Python. This would greatly simplify distribution and installation of this system.

There is a file included with this codebase called `pyinstaller/roastgui.spec`.  This is a configuration file for pyinstaller that may successfully produce the codebase into a distributable package.  While there has been success on Linux, further testing on other operating systems have not been tested completely.

Preliminary testing has been done on Windows using a [Cygwin](http://cygwin.com/) environment to compile the roast toolchain. There are a few apparent problems such as roast failing when trying to invoke `rm -f outputfile.maf.`  This indicates that further modification of the roast toolchain will be required before it runs on Cygwin.

Troubleshooting the roast toolchain
----------------------------------------------

** Roast toolchain modification**  
During the development of this application: there was a small modification made to the roast codebase.  the modification causes the invocation of helper executables to be non-relative to the current working directory.  With this modification: the invocation of the helper tools are searched by the PATH environment variable.  This change is available on the [roast repository](https://bitbucket.org/scvnc/roast) via the `vincent_mod` branch.  When using the `build.py` script: it will automatically obtain this version.  

** GCC bug**  
Due to a bug on the latest version of the gcc compiler, it improperly causes warnings for unused variables when compiling roast.  Since the Makefile compiles the toolchain with the `-Werror` flag, this causes all warnings to be treated as compilation errors.  Thus compiling will fail on newer versions of gcc.  Upon removing this compile flag in the makefile; it compiles successfully on newer versions of gcc.



Authors
-------------
The initial roastGUI codebase was written by Vincent Schramer, a research assistant student at Northern Illinois University. Feel free to contact Vincent by e-mail (vinciple at gmail dot com) with further questions and feature requests. Additionally, Vincent is subscribed to the issue tracker on this repository and encourages the submission of feature requests or bug reports.
