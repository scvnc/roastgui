"""
Workers.py

This is a file from an earlier era of the appliaction and should be
wholly depricated. It's functionality is now replaced with classes
such as AllBZController, RoastController, RoastJob, AllBZJob.

It should be considered for deletion.

@author Vincent Schramer
"""

from PyQt4.QtCore import QThread
from PyQt4.QtCore import SIGNAL
from subprocess import Popen, PIPE
from time import sleep
from Wrappers.Workspace import Workspace
import os


class Worker(QThread):
    
    def __init__(self, parent = None):
        QThread.__init__(self, parent)
        
        self.exiting = False
        
        self.cmd = []
        
        # SIGNALS
        self.line_signal = SIGNAL("line(QString)")
        self.done_signal = SIGNAL("done()")
    
    def __del__(self):
        
        self.exiting = True
        self.wait()
    
    
    
########################################################################
class RoastWorker(Worker):
    
    def __init__(self, parent = None):
        Worker.__init__(self, parent)
        
    
    def invoke(self, cmd):
        
        self.cmd = cmd;
        
        self.run()
    
    def run(self):
        
        roast = Popen(self.cmd) #, stdout=PIPE, stdin=PIPE, stderr=PIPE)
        
        # While roast has not exited
        while roast.poll() == None:
            
            """
            # Read a line
            line = roast.stdout.readline()
            
            # If there actually are lines
            while len(line) != 0:
                print 'reading errline'
                # Emit the line
                self.emit(self.line_signal, line)
                
                # Try reading another line and continue loop
                line = roast.stdout.readline()
            
            
            errline = roast.stderr.readline()
            while len(errline) !=0:
                print 'reading errline'
                
                self.emit(self.line_signal, line)
                errline = roast.stdout.readline()
            """
            # Sleep if there are no lines to read.
            
            sleep(0.5)
        
        
        # Complete
        print 'done'
        self.emit(self.done_signal)
        
########################################################################

class AllBZWorker(Worker):
    
    
    def __init__(self, parent = None):
        Worker.__init__(self, parent)

        self.species_list = []
        
        self._Popen_cwd = os.getcwd();
        self._Popen_path = None;
        self._cmd = None
        

    
    def _assert_params(self):
        #assert(self._Popen_path != None)
        assert(self._cmd != None)
    
    def set_cwd(self, cwd):
        self._Popen_cwd = cwd
        
    
    def set_path(self, path):
        self._Popen_path = path
        
        
    def set_cmd(self, cmd):
        self._cmd = cmd
        
    
    def run(self):
        
        self._assert_params()
        
        cmd = Popen(self._cmd, cwd=self._Popen_cwd)
        cmd.wait()
        
        # Complete
        print 'done'
        self.emit(self.done_signal)
        
if __name__ == "__main__":
    
    import sys
    from PyQt4 import QtGui
    
    app = QtGui.QApplication(sys.argv)
    
    window = QtGui.QMainWindow()
    window.show()
    
    all_bz = AllBZWorker()
    all_bz.set_Popen_params(cwd='/', path='.')
    all_bz.start()
    
    sys.exit(app.exec_())
