"""
allbzmain.py

Contains the widget that pertains to creating all_bz jobs.

@author Vincent Schramer
"""

from PyQt4.QtGui import QWidget, QTableWidgetItem, QFileDialog
from PyQt4.QtCore import SIGNAL
from PyQt4.QtCore import pyqtSignal
from Ui_BatchMain import Ui_BatchMain
import os
import shutil
import errno

from JobControl.Jobs.AllBZJob import AllBZJob

class AllBZMain(QWidget, Ui_BatchMain):
    
    # Signal that emitts a job.
    job_created = pyqtSignal(AllBZJob)
    
    def __init__(self, parent=None):
        
        # Call parent constructor.
        QWidget.__init__(self, parent)
        
        # Configure designer-generated UI.
        self.setupUi(self)
        
        self._connect_signals()
        
        # Disable checkbox that wont be used yet.
        self.gen_specfile_cbox.setDisabled(True)
    
    
    def _connect_signals(self):
        """
        Connects the events from the UI to delegate methods.
        """
        
        # Handle click of  "browse for sequenes" button
        self.add_seqs_btn.clicked.connect(self._show_selection_browser)
        
        
        # Handle click of "browse for specfile" button 
        self.specify_specfile_btn.clicked.connect(self._show_specfile_browser)
     
        self.specify_output_btn.clicked.connect(self._show_output_browser)
        
        # Handle click of the start job button.
        self.begin_batch_btn.clicked.connect(self._handle_begin_btn_clicked)
        


    def _show_output_browser(self):
        
            # Show dialog to select output folder
        folder = QFileDialog.getExistingDirectory(caption='Select FASTA files', 
            directory=os.getcwd())
            
        self.output_dir_line.setText(folder)
    
    def _handle_begin_btn_clicked(self):
        """
        Actions performed when the button for 
        invoking the job is pressed.
        """
        
        # Construct job
        job = self._construct_job()
        
        # Send Job.
        self.job_created.emit(job)
        
        
    
    def _construct_job(self):
        """
        Creates an AllBZ Job based on the current UI values.
        
        Perhaps this is where future exceptions from the AllBZController
        could be caught for validation.
        
        @returns AllBZJob
            Constructed AllBZJob
        """
        # Create an AllBZJob
        
        job = AllBZJob()
        
        # Create species symlinks
        seq_list = self.sequence_table.get_list()
        
        # Add sequences
        for i in seq_list:
            job.all_bz.add_sequence_file(i[0], i[1])
        
        # Set all other values.
        
        job.all_bz.guid_tree = self._obtain_guid_string()
        
        job.all_bz.reference_species = self._obtain_reference()
        
        job.all_bz.output_dir = self._obtain_out_dir()
        
        job.all_bz.specfile = self._obtain_specfile()
        
        return job
        
    # # #
    # Functions regarding selection of sequence files.
    #
    
    def _show_selection_browser(self):
        """
        This causes a file browser to open to select one to many 
        sequence files to be inserted into the sequence table.
        """
        # Show dialog to select multiple files
        files = QFileDialog.getOpenFileNames(caption='Select FASTA files', 
            directory=os.getcwd())
        
        # Pass the list of files to be handled.
        self.add_seqs(files)
        
    def _show_specfile_browser(self):
        """
        This causes a file browser to open to select a specfile.
        """
        
        # Show dialog to select a single file.
        specfile = QFileDialog.getOpenFileName(caption='Select specfile', 
            directory=os.getcwd())
        
        # Change UI widget that represents path for specfile
        # to the resulting file.
        self.specfile_line.setText(specfile)
        
    
    def add_seq(self, pathname, species=None):
        """
        Add a pathname representing a sequence to the table of seqs.

        @param str pathname
            The path to the sequence file.
            
        @param str species
            The name of the species that the sequence file represents.
        """
        
        # Convert to Python string.
        pathname = str(pathname)
        
        # If a particular species name isn't provided, use the 
        # non-extension part of the path.
        if species==None:
            species = os.path.basename(pathname).split(".")[0]
        
        # Add it to the sequence table.
        self.sequence_table.add_sequence(pathname, species)
    
    
    def add_seqs(self, pathnames):
        """
        Add a list of pathnames to the table of sequences.
        
        
        @param List pathnames
            An iteratable list of path names for sequences.
        """
        
        for i in pathnames:
            self.add_seq(i)
    
    #
    # # # # 
    # Methods regarding the retrieval of values from the UI.
    
    def _obtain_guid_string(self):
        return str(self.guid_line.text())

    
    def _obtain_reference(self):
        return str(self.ref_line.text())
    
    
    def _obtain_specfile(self):
        return str(self.specfile_line.text())
        
    
    def _obtain_out_dir(self):
        
        dir = str(self.output_dir_line.text())
        
        # Make directory
        try:
            os.makedirs(dir)
        except OSError as exc: # Python >2.5
            if exc.errno == errno.EEXIST and os.path.isdir(dir):
                pass
            else: raise
        
        return dir
        
    
if __name__ == "__main__":

    import sys

    from PyQt4 import QtGui


    app = QtGui.QApplication(sys.argv)
    
    a = AllBZMain()
    a.show()
    
    def x(job):
        print job
    
    


    sys.exit(app.exec_())
