"""
joblistwidget.py

This is a subclassing of a QAbstractListView and a QListView in order 
to make a List Widget 'view' for a JobManager.


@author Vincent Schramer

"""

from PyQt4.QtGui import QWidget
from PyQt4.QtGui import QListWidget
from PyQt4.QtGui import QListWidgetItem
from PyQt4.QtCore import QAbstractListModel
from PyQt4.QtCore import QModelIndex
from PyQt4.QtCore import QVariant
from PyQt4.QtCore import Qt
from PyQt4.QtCore import pyqtSignal
from PyQt4.QtGui import QListView

from JobControl.JobManager import JobManager
from JobControl.Jobs.Job import Job


class JobListWidget(QListView):
    """
    The JobListWidget will display a listing of jobs within a JobManager
    
    There may be some problems between python threads
    and QThreads that I'm anticipating. UPDATE: yes, there are problems,
    but seem to be resolved by using thread-safe queues to communicate
    between the QT UI loop and python threads.
    
    """
    
    # Signal that is emitted when a job is selected.
    jobSelected = pyqtSignal(Job)
    
    
    def __init__(self, parent=None, *args):
        QListWidget.__init__(self, parent, *args)
        
    def select_row(self, row):
        index = self._model.createIndex(row,0)
        self.setCurrentIndex(index)
    
    # job_manager property
    @property
    def job_manager(self):
        """ job_manager Accessor """ 
        return self._job_manager
        
    @job_manager.setter
    def job_manager(self, jm):
        """ 
        job_manager setter
        This also does some housekeeping along with holding
        a reference to the job_manager.
        """
        
        # Assign data member reference of the job manager.
        self._job_manager = jm
        
        # Instantiate a model based around this job manager.
        self._model = JobManagerModel(self._job_manager)
        
        # Set this QListView's model to the newly created model.
        self.setModel(self._model)
        
        # Assign handler to index click.
        self.clicked.connect(self._handle_index_moved)
    
    
    def _handle_index_moved(self, IndexObj):
        """
        This handler should be invoked when the selected index of
        the QListView has changed. It should emitt the selected_job
        signal.
        """
        
        if self.job_manager != None:
            
            selected_job = self._model.data(IndexObj, Qt.UserRole)
            
            self.jobSelected.emit(selected_job)
        
    
    
        

class JobManagerModel(QAbstractListModel):
    
    """
    This is a subclassed model for displaying 
    job information in a JobManager.
    
    TODO: Load all current jobs from the JobManager.  As of now, 
    this model only subscribes to the on_queue_update list on a
    JobManager.  This should be no big deal for the GUI however.
    """
    
    
    def __init__(self, job_manager, parent=None, *args): 
        
        QAbstractListModel.__init__(self, parent, *args)
        
        # This model actually maintains an internal list of Jobs.
        self._internal_list = []
        
        # Subscribe to the queue_update event in the job_manager
        job_manager.on_queue_update.append(self._handle_job_insertion)

 
 
    def rowCount(self, parent=QModelIndex()):
        """
        Needed implementation for list model subclassing,
        returns the number of items in the list.
        """
        return len(self._internal_list)
    
    
    def data(self, index, role): 
        """
        Accesses data from the model, given a QModelIndex() and the role
        of the item.
        """
        if index.isValid():
            
            # Are we accessing the display string or the job itself?
            if role == Qt.DisplayRole:
                return self._internal_list[index.row()].name
                
            elif role == Qt.UserRole:
                return self._internal_list[index.row()]
                
        else:
            
            # Return invalid QVariant.
            return QVariant()


    def _handle_job_insertion(self, job):
        """
        Should be called when a job is inserted by a JobManager.
        Inserts a row into this model's internal list and emitts the
        correct signals for the view to update.
        """
        self.beginInsertRows(QModelIndex(), 0, 0)
        
        self._internal_list.insert(0, job)
        
        self.endInsertRows()



if __name__ == "__main__":

    import sys

    from PyQt4 import QtGui
    
    from JobControl.Jobs.SleepJob import SleepJob


    
    job_manager = JobManager()
    
    app = QtGui.QApplication(sys.argv)

    a = JobListWidget()
    
    a.job_manager = job_manager
    
    def x(job):
        print job
        
    a.jobSelected.connect(x)
    
    a.show()
    

    
    job_manager.add_job(SleepJob())
    job_manager.add_job(SleepJob())
    
    
    

    sys.exit(app.exec_())
