"""
fileselectorgroupbox.py

Contains the FileSelectorGroupBox widget

@author Vincent Schramer
"""

import os
from PyQt4.QtGui import QGroupBox, QListWidgetItem, QFileDialog
from Ui_FileSelectorGroupBox import Ui_FileSelectorGroupBox 

class FileSelectorGroupBox(QGroupBox, Ui_FileSelectorGroupBox):
    
    """
    An attempt to make a reusable widget for selecting files.
    Currently only used in the RoastMain widget :(
    """
    
    def __init__(self, parent=None):
        QGroupBox.__init__(self, parent)
        self.setupUi(self)
        
        self._connect_signals()
        
        # Default properties
        self._select_filter = "Multiple Alignment Format (*.maf *.Maf *.MAF)"
        
        self._browser_caption = "Select files to process."
    
        
    
    def set_select_filter(self, string):
        self._select_filter = string
    
    pass
    
    
    def _connect_signals(self):
        
        self.add_btn.clicked.connect(self.show_selection_browser)
        
        pass
    
    # # #
    # Functions regarding selection of maf_source folder and a reference species.
    #
    
    def show_selection_browser(self):
        
        """ 
        Displays a file browser for selecting maf files. 
        """
        
        files = QFileDialog.getOpenFileNames(caption=self._browser_caption, 
            directory=os.getcwd(),
            filter=self._select_filter)
            
        self.add_mafs(files)
        
                                
    
                                           
    def add_maf(self, pathname):
        """ Adds a single maf file to the list
            
            TODO: Validation of valid path?
            
            @param str pathname
                The full path to the maf file.
        """
        
        # Obtain the basename.
        item = QListWidgetItem(os.path.basename(str(pathname)))
        
        # Also store the full pathname
        item.fullpath = pathname
        
        # Add it to the collection
        self.file_list.addItem(item)
        
        
        
        
    
    def add_mafs(self, pathnames):
        """
        takes a list of pathnames and calls add_maf for each one.
        
        @param str pathnames
            List of full pathnames.
        """
        
        for i in pathnames:
            self.add_maf(i)
        
        
        
    def obtain_maf_source_string(self):
        
        list = []
        
        
        for n in self.get_maf_sources():
            list.append(str(n.fullpath))
            
        return list
    
    
    def get_maf_sources(self):
        
        list = []
        
        count = self.file_list.count()
        for n in range(0, count):
            list.append(self.file_list.item(n))
        
        return list
    
    
    def update_species_list(self):
        
        """ MAY BE DEPRICATED
            This tries to obtain a possible set of species from the 
            collection of maf files in order to populate the reference
            combobox.  This is probably flawed as we can't guarentee
            the filename to be normalized.  TODO: open the first few
            lines of the file to guess what's in it?"""
            
        species_set = set()
        
        # For each maf filename
        for i in self.get_maf_sources():
            
            # Split the maf file by it's dot extensions
            parts = str(i.text()).split('.')
            
            # For each part of the filename
            #for p in parts:
                
                # Ignore if it is in the blacklist
                #if (p.lower() in set(('roast','sing','toast','maf'))):
                    #continue
                
            # Add species to the set.
            species_set.add(parts[0].lower())
            species_set.add(parts[1].lower())
        
        # Clear the combobox.
        for i in range(0, self.reference_combox.count()):
            self.reference_combox.removeItem(0)
        
        # Add the set of species.
        for s in species_set:
            self.reference_combox.addItem(s)
            
        
        
        
    
    #
    # # # # # #
    #
    #
    
if __name__ == "__main__":

    import sys

    from PyQt4 import QtGui


    app = QtGui.QApplication(sys.argv)

    a = FileSelectorGroupBox()
    a.show()


    sys.exit(app.exec_())

