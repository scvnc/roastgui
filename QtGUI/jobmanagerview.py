"""
JobManagerView

This widget is intended to be used as a tab in the main interface 
that handles the viewing of running jobs from the current application
session.


@author Vincent Schramer

"""

import os
from PyQt4.QtGui import QWidget, QFileDialog
from PyQt4.QtGui import QTextCursor
from PyQt4 import QtCore
from PyQt4.QtCore import QTimer
from PyQt4.QtCore import QThread
from Ui_JobManagerView import Ui_JobManagerView
from Queue import Queue
from JobControl.Jobs.Job import Job

            

class JobManagerView(QWidget, Ui_JobManagerView):
    """
    The JobManagerView is a view for a JobManager class.
    
    When using it in the interface, you must set the job_manager 
    property to an instance of a JobManager.  It will display new jobs
    a QListView and allow one to see the progress of it's stdout
    (and eventually stderr).
    """
    
    
    def __init__(self, parent=None):
        """ Constructor for the main window of Roast"""
        
        # Call parent constructor.
        QWidget.__init__(self, parent)
        
        # This maintains which job is currently being displayed.
        self._active_job = None
        
        
        # qt-designer initialization
        self.setupUi(self)
        
        
        # Turn off the progress bar
        self._set_progress_bar_minimum()
        

        # Set up UI signals.
        self._connectObjects()
        
        
        # This is a thread-safe queue for non-gui threads to write to
        # for new lines of output.
        self.output_queue = Queue()
        self.error_queue = Queue()
        
        # Initialize the timer that continuously polls the output_queue.
        self._initialize_output_timer()
        

    
    #
    # # #
    # Methods related to managing the progress bar widget.
    
    def _set_progress_bar_complete(self):
        
        # Max progress bar
        self.progress_bar.setMaximum(100)
        self.progress_bar.setValue(100)
        
    
    def _set_progress_bar_minimum(self):
        
        self.progress_bar.setMaximum(100)
        self.progress_bar.setValue(0)
    
    def _set_progress_bar_working(self):
        
        # Cause the progress bar to display the busy animation
        # by setting the min/max to 0
        self.progress_bar.setMaximum(0)
        self.progress_bar.setValue(0)
    
    #
    # # #
    #
    
    def _scroll_area_to_end(self, scroll_area):
        """
        Given a scroll area, like a QPlainTextEdit, this will scroll
        down to the bottom.
        """
        
        # Obtain the scrollbar object.
        vertical_sb = scroll_area.verticalScrollBar()
        
        # Set it's value to the max.
        vertical_sb.setValue(vertical_sb.maximum())
    
    
    def _initialize_output_timer(self):
        
        self._timer = QTimer()
        
        # Poll 10 times per second.
        self._timer.setInterval(100)
        
        # Tell the timer what to invoke.
        self._timer.timeout.connect(self._job_status_poller)
        
        self._timer.start()
    
    
    def _connectObjects(self):
        """ Connects signals from events to functions. """
        self.job_list.jobSelected.connect(self._handle_select_job)
        

    
    # Job Manager Property.
    @property
    def job_manager(self):
        return self._job_manager
        
    @job_manager.setter    
    def job_manager(self, jm):
        """
        Pass in the job manager to be managed by the list widget and
        subscribe to it's events.
        """
        
        self.job_list.job_manager = jm
        
        # Subscribe to job done event on the job manager.
        jm.on_job_done.append(self._handle_job_done)


    
    #
    # # # # # #
    # Event Handlers
    #
    
    
    def _handle_job_done(self, job):
        """
        Not using this.. need to remove this and the subscription chain.
        Doing things this way would update the UI in a non UI thread.
        """
        pass
            
            
            
    def _handle_select_job(self, job):
        """
        This should be called when a new job is selected.  It handles
        displaying it's progress.
        
        @param Job job
            Reference to the job that was just selected.
        """
        # If there is an active job on the view, 
        # unrsubscribe it from the on_progress_text_update event
        if self._active_job:
            
            self._active_job.on_progress_text_update.remove(
                self.output_queue.put)
                
            self._active_job.on_error_text_update.remove(
                self.error_queue.put)
        
        # For this newly selected job, set it as the
        # actively displayed job.
        self._active_job = job
        
        # Display the progress_text of the job in the output pane.
        self.output_textbox.setPlainText(self._active_job.progress_text)
        self.error_textbox.setPlainText(self._active_job.error_text)
        
        
        
        self._scroll_area_to_end(self.output_textbox)
        self._scroll_area_to_end(self.error_textbox)
        
        
        
        # Subscribe to progress_text updates, having new lines placed
        # in to the output_queue.
        job.on_progress_text_update.append(self.output_queue.put)
            
        job.on_error_text_update.append(self.error_queue.put)
        
    
    def _job_status_poller(self):
        """
        This should be called in intervals by the GUI in order to see
        if there are any new lines in the output_queue to display.
        
        """
        
        # Quit if there is no active job.
        if (self._active_job == None):
            return
        
        if self._active_job.status == Job.status_enums["COMPLETE"]:
            self._set_progress_bar_complete()
            
        elif self._active_job.status == Job.status_enums["RUNNING"]:
            self._set_progress_bar_working()
        
        else:
            self._set_progress_bar_minimum()
        
        try:
            
            # Attempt to get a line from the queue. (blocking == false)
            self.output_queue.get(False)
            
            # TODO: better line handling
            # Right now, we are just popping the line from the queue
            # and obtaining the entire output string from the job.
            # 
            # One way of doing this appears to be manipulating
            # a QTextCursor and inserting a line.
            
            self.output_textbox.setPlainText(self._active_job.progress_text)
            
            self._scroll_area_to_end(self.output_textbox)
            
        except:
            # If the queue is empty, it will throw an exception.
            # Catch the exception here and keep going.
            pass
            
        try:
            
            self.error_queue.get(False)
        
            self.error_textbox.setPlainText(self._active_job.error_text)
            
            self._scroll_area_to_end(self.error_textbox)
            
        except:
            pass
        
    
    #
    #
    # # # # # #
    #
    

        

if __name__ == "__main__":

    import sys

    from PyQt4 import QtGui
    from JobControl.JobManager import JobManager
    from JobControl.Jobs.SleepJob import SleepJob
    from time import sleep
    
    app = QtGui.QApplication(sys.argv)

    a = JobManagerView()
    jm = JobManager()
    a.job_manager = jm
    a.show()
    
    for i in range(0, 10):
        
        
        job = SleepJob()
        job.time = 10
        
        jm.add_job(job)

    sys.exit(app.exec_())

        
