"""
sequencetablewidget.py

Contains the SequenceTableWidget class.

@author Vincent Schramer.
"""


from PyQt4.QtGui import QWidget, QTableWidget, QTableWidgetItem, QRadioButton


class SequenceTableWidget(QTableWidget):
    
    """
    This is a subclass of the QTableWidget provided by the Qt framework
    It is to maintain a table of species names and their coorisponding
    path to a fasta sequence file.  This allows one to select their 
    desired FASTA file and then provide the species name that corellates
    to the species name in the guid tree.
    
    For example, I could have a FASTA file called baboon-2013-01-01.fa
    that I want to process with all_bz.  Once I select this file with
    the file manager, it ought to be available in this table widget. 
    From there, I can edit the species name to be simply "baboon" 
    instead of baboon-2013-01-01.  This allows me to construct my GUID
    tree string as: (((human chimp) baboon) (mouse rat)).  Noting that
    I have typed baboon.
    """
    
    
    def __init__(self, *args):
        QTableWidget.__init__(self, *args)
        
        # Currently only two columns
        self.setColumnCount(2);
        
        # Set header names
        self.setHorizontalHeaderItem(0, QTableWidgetItem("Name"))
        self.setHorizontalHeaderItem(1, QTableWidgetItem("Path"))
        

    def add_sequence(self, path, name='None'):
        """
        Given a path and a name, this will add the sequence into the 
        table.
        """
        
        # Obtain last row index.
        pos = self.rowCount()
        
        # Make a new row.
        self.insertRow( pos )
        
        # Set the first column to be the name.
        self.setItem(pos, 0, QTableWidgetItem(name))
        
        # Set the second column to be the path string.
        self.setItem(pos, 1, QTableWidgetItem(path))
        
        
        
    def get_list(self):
        """
        This returns a list containing each entry in the table.
        An entry is simply another list containing each column.
        
          name     path
        | baboon | /path/to/baboon.fa |
        
        Will translate to...
        
        [ ['baboon', '/path/to/baboon.fa'] ]
        
        """
        
        list = []
        
        for r in range(0, self.rowCount()):
            
            list.append([])
            
            for c in range(0, self.columnCount()):
                
                list[r].append(str(self.item(r, c).text()))
        
        return list
    
        
    def del_sequence(self, row):
        """
        Not implemented
        """
        pass

if __name__ == "__main__":

    import sys

    from PyQt4 import QtGui


    app = QtGui.QApplication(sys.argv)

    a = SequenceTableWidget()
    a.show()
    
    a.add_sequence("Asfd");
    a.add_sequence("Asfd");

    a.get_list();

    sys.exit(app.exec_())
