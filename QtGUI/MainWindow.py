"""
MainWindow.py

This module contains the class which is the primary Qt window for the 
PyQt4 roastGUI.

@author Vincent Schramer
"""
import os
import sys


# If this isn't a frozen Python app...
# (This may need to be adjusted when making this app compatible with
# pyinstaller.)
if not getattr(sys, 'frozen', None):
    script_path = os.path.dirname(os.path.abspath(__file__))
    sys.path.append( script_path + os.path.sep + 'designer' )

from time import sleep

from PyQt4 import QtGui, uic
from PyQt4 import QtCore
from PyQt4.QtCore import QStringList
from PyQt4.QtGui import QMainWindow, QDialog, QFileDialog, QStringListModel, QListView, QListWidgetItem, QWidget
from Ui_MainWindow import Ui_MainWindow

from JobControl.JobManager import JobManager


from Ui_RoastMain import Ui_RoastMain


class MainWindow(QMainWindow, Ui_MainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()
        
        
        # Instantiate the job manager
        self.job_manager = JobManager() 
        
        # Set up the user interface from Designer.
        self.setupUi(self)
        
        # Pass reference of the job_manager to the jobs_tab
        self.jobs_ui.job_manager = self.job_manager
        
        # Subscribe to roast_ui, passing jobs it creates
        # to the handle_job method.
        self.roast_ui.on_job_created.append(self.handle_job)
        
        self.all_bz_ui.job_created.connect(self.handle_job)
        
    
    
    def handle_job(self, job):
        """
        This should be invoked when a new job is created by a child
        widget.  
        
        For example, the roast_ui will generate a Job and then
        emitt it.  Since this method is subscribed to the emission,
        it will be invoked and handle the job.
        
        It causes the active tab to be switched to the 
        view that displays jobs and the job to be added to the 
        JobManager.
        """
        
        self.job_manager.add_job(job)
        
        
        # Switch tab to the JobManagerView which should be on index 2
        self.step_tabs.setCurrentIndex(2);
        
        # Attempting to also select the new job's row
        # currently not working.
        
        #self.jobs_ui.job_list.select_row(0)
        
        

    
    
