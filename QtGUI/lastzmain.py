"""
lastzmain.py

Skeleton for a LASTZ job creation widget.

Not implemented.

@author Vincent Schramer
"""

@author Vincent Schramer
from PyQt4.QtGui import QWidget
from Ui_LastzMain import Ui_LastzMain 

class LastzMain(QWidget, Ui_LastzMain):
    
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        self.setupUi(self)
    
    pass
