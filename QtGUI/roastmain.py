"""
roastmain.py

Contains the roastmain class (QWidget)

@author Vincent Schramer
"""

import os
from PyQt4.QtGui import QWidget, QFileDialog
from PyQt4 import QtCore
from Ui_RoastMain import Ui_RoastMain 

from Wrappers.RoastController import RoastController
from JobControl.Jobs.RoastJob import RoastJob

class RoastMain(QWidget, Ui_RoastMain):
    """
    This Qt Widget is a set of controls which manage the user interface
    for creating and submitting RoastJobs.
    """
    
    def __init__(self, parent=None):
        """ Constructor for the main window of Roast"""
        

        
        # Call parent constructor.
        QWidget.__init__(self, parent)
        
        # # # # 
        # Event Subscriptions
        
        self.on_job_created = []
        print self.on_job_created
        
        # # # #
        
        # qt-designer initialization
        self.setupUi(self)
        
        self.roast_job = RoastJob()
        
        # Instantiate a roast controller.
        self.roast = self.roast_job.roast
    
        # Fill in defaults for files.
        dest_dir = os.getcwd()
        self.roast.output_file = dest_dir + '/output.maf'
        
        # Set up signals.
        self._connectObjects()

             
    def _connectObjects(self):
        """ Connects signals from events to functions. """
        
        self.select_destination_btn.clicked.connect(self._call_destination_browser)
        
        self.roast_btn.clicked.connect(self.roast_btn_clicked)

        
    
    def _call_destination_browser(self):
        """ Brings up a file browser for determining the output file
            and saves the result in the destination_path line edit """
            
        path = QFileDialog.getSaveFileName(caption="Select roast output file")
        self.destination_path.setText(path)

        
    
    
    
    #
    # # # # # 
    # Events
    #
    
    def _signal_job_created(self, job):
        
        for delegate in self.on_job_created:
            delegate(job)
    
    #
    # # # # # #
    #
    #
    
    def roast_btn_clicked(self):
        """ 
        This is the event handler which causes the invocation of roast.
        """
        
        self._configure_roast_controller()
        
        # Send job to subscribers.
        self._signal_job_created(self.roast_job)
        
        # Create new instance of RoastJob
        self.roast_job = RoastJob()
    
    
    #
    #
    # # # # # #
    #
    
    def _configure_roast_controller(self):
        """
        Configures the instance of the roast controller with values
        from the user interface.
        """
        
        # Obtain reference to the job's roast controller.
        roast = self.roast_job.roast
        
        # Add each sequence file to the roast controller.
        for i in self.seq_group_box.obtain_maf_source_string():
            roast.add_sequence_file(i)
        
        #strand
        roast.strand = str(self.strand_combox.currentText())
        
        #dynamic programming radius
        roast.dpr = int(str(self.DPR_txt.text()))
        
        #minimum block length
        roast.min_len = int(str(self.min_block_len_txt.text()))
        
        #refrence species
        roast.reference_species = str(self.reference_combox.currentText())
        
        # GUID Tree
        roast.guid_tree = str(self.guid_tree_txt.text())
        
        # Destination
        roast.output_file = str(self.destination_path.text())
        

if __name__ == "__main__":

    import sys

    from PyQt4 import QtGui


    app = QtGui.QApplication(sys.argv)

    a = RoastMain()
    a.show()


    sys.exit(app.exec_())

        
