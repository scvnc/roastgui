#!/usr/bin/env python

"""
roastgui.py

This is the main entry point for the desktop version of roastGUI.

@author Vincent Schramer
"""

import sys
import os

# If this isn't a frozen Python app...
# (frozen means that it's been packaged up with pyinstaller)
if not getattr(sys, 'frozen', None):
    
    # Configure some module directories.
    
    # Obtain the path where this script is located.
    script_path = script_path = os.path.dirname(os.path.abspath(__file__))
    
    # Add the following module directories to Python's module PATH.
    sys.path.append( script_path + os.path.sep + 'QtGUI' )
    sys.path.append( script_path + os.path.sep + 'lib' )

from PyQt4 import QtGui
from MainWindow import MainWindow

# Add roast toolchain folder to system PATH. 
os.environ["PATH"] += os.pathsep + os.path.dirname(os.path.abspath(__file__)) + '/lib/roast'

# Initialize the base Qt4 Application class.
app = QtGui.QApplication(sys.argv)

# Instantiate the main window and then show it.
main_window = MainWindow()
main_window.show()


# Begin execution of the Qt4 application and use it's return code
# as the script's return code.
sys.exit(app.exec_())
